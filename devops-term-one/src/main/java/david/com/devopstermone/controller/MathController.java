package david.com.devopstermone.controller;
import david.com.devopstermone.dto.DoMathRequest;
import david.com.devopstermone.exception.InvalidOperationException;
import david.com.devopstermone.service.MathService;
import org.springframework.web.bind.annotation.*;
@RestController
public class MathController {
    MathService mathService;
    @PostMapping("/math")
    public double doMath(@RequestBody DoMathRequest doMathRequest) throws InvalidOperationException {
        return mathService.doMath(doMathRequest.getOperand1(), doMathRequest.getOperand2(), doMathRequest.getOperator());
    }
}
