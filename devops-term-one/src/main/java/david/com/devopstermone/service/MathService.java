package david.com.devopstermone.service;

import david.com.devopstermone.exception.InvalidOperationException;
public class MathService {
    public double doMath(double operand1, double operand2, String operator) throws InvalidOperationException {
        double result = 0;
        if ("/".equals(operator) && operand2 == (double) 0) {
            throw new InvalidOperationException("Cannot divide by zero");
        }
        else{
            //use if statements to check for the operator
            if ("+".equals(operator)) {
                result = operand1 + operand2;
            }
            else if ("-".equals(operator)) {
                result = operand1 - operand2;
            }
            else if ("*".equals(operator)) {
                result = operand1 * operand2;
            }
            else if ("/".equals(operator)) {
                result = operand1 / operand2;
            }
            else if("**".equals(operator)){
                result = Math.pow(operand1, operand2);
            }
            else if("log".equals(operator)){
                result=operand1*Math.log10(operand2);
            }
            else if("ln".equals(operator)){
                result= operand1*Math.log(operand2);
            }
            else {
                throw new InvalidOperationException("Unknown operation");
            }
            return result;
        }
    }
}

