package david.com.devopstermone.service;

import david.com.devopstermone.exception.InvalidOperationException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
@RunWith(SpringRunner.class)
public class MathServiceTest {
    //building tests for doMath method
    @Test
    //doMath method should add two numbers
    public void doMathShouldAddTwoNumbers() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "+";
        double expected = 5;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should subtract two numbers
    public void doMathShouldSubtractTwoNumbers() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "-";
        double expected = -1;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should multiply two numbers
    public void doMathShouldMultiplyTwoNumbers() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "*";
        double expected = 6;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should divide two numbers
    public void doMathShouldDivideTwoNumbers() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "/";
        double expected = 0.6666666666666666;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should raise one number to the power of another
    public void doMathShouldRaiseOneNumberToThePowerOfAnother() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "**";
        double expected = 8;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should calculate the logarithm of one number to the base of another
    public void doMathShouldCalculateTheLogarithmOfOneNumberToTheBaseOfAnother() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "log";
        double expected = 0.6309297535714574;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should calculate the natural logarithm of one number to the base of another
    public void doMathShouldCalculateTheNaturalLogarithmOfOneNumberToTheBaseOfAnother() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "ln";
        double expected = 0.6931471805599453;
        MathService mathService = new MathService();
        double actual = mathService.doMath(operand1, operand2, operator);
        assertEquals(expected, actual);
    }
    @Test
    //doMath method should throw an exception if the operator is unknown
    public void doMathShouldThrowAnExceptionIfTheOperatorIsUnknown() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 3;
        String operator = "unknown";
        MathService mathService = new MathService();
        InvalidOperationException thrown = org.junit.jupiter.api.Assertions.assertThrows(InvalidOperationException.class, () -> mathService.doMath(operand1, operand2, operator));
        assertEquals("Unknown operation", thrown.getMessage());
    }
//test if operand2 is 0 and operator is /
    @Test
    //doMath method should throw an exception if the operator is unknown
    public void doMathShouldThrowAnExceptionIfTheOperatorIsDivideAndOperand2IsZero() throws InvalidOperationException {
        double operand1 = 2;
        double operand2 = 0;
        String operator = "/";
        MathService mathService = new MathService();
        InvalidOperationException thrown = org.junit.jupiter.api.Assertions.assertThrows(InvalidOperationException.class, () -> mathService.doMath(operand1, operand2, operator));
        assertEquals("Cannot divide by zero", thrown.getMessage());
    }
}
