package david.com.devopstermone.controller;

import david.com.devopstermone.dto.DoMathRequest;
import david.com.devopstermone.exception.InvalidOperationException;
import david.com.devopstermone.service.MathService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
public class MathControllerTest {
    @Mock
    MathService mathService;
    @InjectMocks
    MathController mathController;
    @Test
    public void testMathController() throws InvalidOperationException {
        DoMathRequest doMathRequest = new DoMathRequest(1.8,2.2,"+");
        double result = 4.0;
        double actual=mathController.doMath(doMathRequest);
        assertEquals(result,actual);
    }
}
